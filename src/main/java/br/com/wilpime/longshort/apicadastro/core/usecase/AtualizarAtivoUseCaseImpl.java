package br.com.wilpime.longshort.apicadastro.core.usecase;

import java.util.Optional;

import org.springframework.stereotype.Service;

import br.com.thimilo.longshort.base.dto.response.ListaErroEnum;
import br.com.thimilo.longshort.base.dto.response.ResponseDataErro;
import br.com.thimilo.longshort.base.gateway.SalvarGateway;
import br.com.wilpime.longshort.apicadastro.core.entity.Ativo;
import br.com.wilpime.longshort.apicadastro.core.usecase.dto.AtivoRequest;
import br.com.wilpime.longshort.apicadastro.core.usecase.dto.AtivoResponse;
import br.com.wilpime.longshort.apicadastro.core.usecase.gateway.BuscarPorCodigoAtivoGateway;

@Service
public class AtualizarAtivoUseCaseImpl extends BaseAtivoUseCase implements AtualizarAtivoUseCase {
	

	private final SalvarGateway<Ativo> gateway;
	private final BuscarPorCodigoAtivoGateway buscarPorCodigo;
	
	public AtualizarAtivoUseCaseImpl(SalvarGateway<Ativo> gateway, BuscarPorCodigoAtivoGateway buscarPorCodigo) {
		super();
		this.gateway = gateway;
		this.buscarPorCodigo = buscarPorCodigo;
	}

	@Override
	public AtivoResponse executar(AtivoRequest input) {
		log.info("Iniciando atualização do ativo de código " + input.getCodigo() + " para descrição");
		AtivoResponse response = new AtivoResponse();
		response.setCodigo(input.getCodigo());
		response.setDescricao(input.getDescricao());
		
		this.validarCamposObrigatorio(input, response);
		
		if(response.getResponse().getErros().size() > 0) {
			return response;
		}
		Optional<Ativo> opAtivo = buscarPorCodigo.buscar(input.getCodigo());
		if(opAtivo.isPresent()) {
			Ativo ativo = opAtivo.get();
			ativo.setDescricao(input.getDescricao());
			ativo = gateway.salvar(ativo);
			response.setId(ativo.getId());
		}
		else {
			String msg = "Ativo não encontrado " + input.getCodigo();
			log.error(msg);
			response.getResponse().adcionarErros(new ResponseDataErro(msg,
					ListaErroEnum.ENTIDADE_NAO_ENCONTRADA ));
		}
		return response;
	}
	

}
