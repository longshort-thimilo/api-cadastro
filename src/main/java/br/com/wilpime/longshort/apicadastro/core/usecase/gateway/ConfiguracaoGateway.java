package br.com.wilpime.longshort.apicadastro.core.usecase.gateway;

public interface ConfiguracaoGateway {
	
	Integer getQuantidadeRegistrosPorPagina();
}
