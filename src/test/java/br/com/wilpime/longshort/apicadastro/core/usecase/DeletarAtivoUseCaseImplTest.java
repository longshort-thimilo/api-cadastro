package br.com.wilpime.longshort.apicadastro.core.usecase;

import java.util.Optional;

import org.junit.Assert;
import org.junit.BeforeClass;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.MockitoJUnitRunner;

import br.com.six2six.fixturefactory.Fixture;
import br.com.six2six.fixturefactory.loader.FixtureFactoryLoader;
import br.com.thimilo.longshort.base.dto.response.ListaErroEnum;
import br.com.thimilo.longshort.base.gateway.DeletarGateway;
import br.com.wilpime.longshort.apicadastro.core.entity.Ativo;
import br.com.wilpime.longshort.apicadastro.core.template.AtivoTemplateLoader;
import br.com.wilpime.longshort.apicadastro.core.usecase.dto.AtivoResponse;
import br.com.wilpime.longshort.apicadastro.core.usecase.gateway.BuscarPorCodigoAtivoGateway;

@RunWith(MockitoJUnitRunner.class)
public class DeletarAtivoUseCaseImplTest {
	
	@InjectMocks
	private DeletarAtivoUseCaseImpl useCase;
	
	@Mock
	private DeletarGateway<Ativo> gateway;
	
	@Mock
	private BuscarPorCodigoAtivoGateway buscarPorCodigoAtivoGateway;
	
	@BeforeClass
	public static void beforeClass() {
		FixtureFactoryLoader.loadTemplates("br.com.wilpime.longshort.apicadastro.core.template");
	}
	
	@Test
	public void deve_deletar_ativo() {
		Ativo ativoPetr4 = Fixture.from(Ativo.class).gimme(AtivoTemplateLoader.PETR4);
		Mockito.when(buscarPorCodigoAtivoGateway.buscar(Mockito.anyString())).thenReturn(Optional.of(ativoPetr4));
		Mockito.when(gateway.deletar(Mockito.anyString())).thenReturn(true);

		
		AtivoResponse response = useCase.executar(ativoPetr4.getCodigo());
		
		Assert.assertEquals(response.getId(), ativoPetr4.getId());
		Assert.assertEquals(response.getCodigo(), ativoPetr4.getCodigo());
		Assert.assertEquals(response.getDescricao(), ativoPetr4.getDescricao());

	}
	
	@Test
	public void nao_deve_deletar_ativo_inexistente() {
		Ativo ativoPetr4 = Fixture.from(Ativo.class).gimme(AtivoTemplateLoader.PETR4);
		Mockito.when(buscarPorCodigoAtivoGateway.buscar(Mockito.anyString())).thenReturn(Optional.empty());
		
		AtivoResponse response = useCase.executar(ativoPetr4.getCodigo());
		
		Assert.assertNull(response.getId());
		Assert.assertEquals(response.getCodigo(), ativoPetr4.getCodigo());
		Assert.assertNull(response.getDescricao());
		Assert.assertEquals(1, response.getResponse().getErros().size());
		Assert.assertEquals(ListaErroEnum.ENTRADA_NAO_ENCONTRADA, response.getResponse().getErros().get(0).getTipo());

	}
	
	@Test
	public void nao_foi_possivel_deletar() {
		Ativo ativoPetr4 = Fixture.from(Ativo.class).gimme(AtivoTemplateLoader.PETR4);
		Mockito.when(buscarPorCodigoAtivoGateway.buscar(Mockito.anyString())).thenReturn(Optional.of(ativoPetr4));
		Mockito.when(gateway.deletar(Mockito.anyString())).thenReturn(false);

		
		AtivoResponse response = useCase.executar(ativoPetr4.getCodigo());
		
		Assert.assertNull(response.getId());
		Assert.assertEquals(response.getCodigo(), ativoPetr4.getCodigo());
		Assert.assertNull(response.getDescricao());
		Assert.assertEquals(1, response.getResponse().getErros().size());
		Assert.assertEquals(ListaErroEnum.NAO_FOI_POSSIVEL_DELETAR, response.getResponse().getErros().get(0).getTipo());

	}
}
