package br.com.wilpime.longshort.apicadastro.core.usecase;

import org.apache.commons.lang.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import br.com.thimilo.longshort.base.dto.response.ListaErroEnum;
import br.com.thimilo.longshort.base.dto.response.ResponseDataErro;
import br.com.wilpime.longshort.apicadastro.core.usecase.dto.AtivoRequest;
import br.com.wilpime.longshort.apicadastro.core.usecase.dto.AtivoResponse;

public abstract class BaseAtivoUseCase {
	
	protected Logger log = LoggerFactory.getLogger(this.getClass());
	
	protected void validarCamposObrigatorio(AtivoRequest input, AtivoResponse response) {
		
		if(StringUtils.isBlank(input.getCodigo()))
		{
			String msg = "O campo código é obrigatório" + input.getCodigo();
			log.error(msg);
			response.getResponse().adcionarErros(new ResponseDataErro(msg,
					ListaErroEnum.CAMPOS_OBRIGATORIOS ));
		}
		if(StringUtils.isBlank(input.getDescricao()))
		{
			String msg = "O campo código é obrigatório" + input.getDescricao();
			log.error(msg);
			response.getResponse().adcionarErros(new ResponseDataErro(msg,
					ListaErroEnum.CAMPOS_OBRIGATORIOS ));
		}
	}

}
