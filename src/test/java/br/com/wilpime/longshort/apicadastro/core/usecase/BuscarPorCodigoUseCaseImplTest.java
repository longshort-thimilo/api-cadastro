package br.com.wilpime.longshort.apicadastro.core.usecase;

import java.util.Optional;

import org.junit.Assert;
import org.junit.BeforeClass;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.MockitoJUnitRunner;

import br.com.six2six.fixturefactory.Fixture;
import br.com.six2six.fixturefactory.loader.FixtureFactoryLoader;
import br.com.thimilo.longshort.base.dto.response.ListaErroEnum;
import br.com.wilpime.longshort.apicadastro.core.entity.Ativo;
import br.com.wilpime.longshort.apicadastro.core.template.AtivoTemplateLoader;
import br.com.wilpime.longshort.apicadastro.core.usecase.dto.AtivoResponse;
import br.com.wilpime.longshort.apicadastro.core.usecase.gateway.BuscarPorCodigoAtivoGateway;

@RunWith(MockitoJUnitRunner.class)
public class BuscarPorCodigoUseCaseImplTest {
	
	@InjectMocks
	private BuscarPorCodigoUseCaseImpl buscarPorCodigoUseCase;
		
	@Mock
	private BuscarPorCodigoAtivoGateway buscarPorCodigoAtivoGateway;
		
	@BeforeClass
	public static void beforeClass() {
		FixtureFactoryLoader.loadTemplates("br.com.wilpime.longshort.apicadastro.core.template");
	}
	
	@Test
	public void deve_buscar_ativo_por_codigo() {
		Ativo ativoPetr4 = Fixture.from(Ativo.class).gimme(AtivoTemplateLoader.PETR4);
		Mockito.when(buscarPorCodigoAtivoGateway.buscar(ativoPetr4.getCodigo())).thenReturn(Optional.of(ativoPetr4));
		AtivoResponse response =  buscarPorCodigoUseCase.executar(ativoPetr4.getCodigo());
		
		Assert.assertNotNull(response);
		Assert.assertEquals(ativoPetr4.getCodigo(),response.getCodigo());
		Assert.assertEquals(ativoPetr4.getId(),response.getId());
		Assert.assertEquals(ativoPetr4.getDescricao(), response.getDescricao());
		Assert.assertEquals(0, response.getResponse().getErros().size());

	}
	
	  @Test
	  public void nao_deve_buscar_ativo_com_codigo_nao_preenchido() {
	    AtivoResponse response = buscarPorCodigoUseCase.executar("        ");
	    
	    Assert.assertNotNull(response);
	    Assert.assertEquals("        ", response.getCodigo());
	    Assert.assertNull(response.getId());
	    Assert.assertNull(response.getDescricao());
	    Assert.assertEquals(1, response.getResponse().getErros().size());
	    Assert.assertEquals(ListaErroEnum.CAMPOS_OBRIGATORIOS, response.getResponse().getErros().get(0).getTipo());
	  }
	  
	  @Test
	  public void nao_deve_retornar_ativo_inexistente() {
	    Ativo ativoPetr4 = Fixture.from(Ativo.class).gimme(AtivoTemplateLoader.PETR4);

	    Mockito.when(buscarPorCodigoAtivoGateway.buscar(ativoPetr4.getCodigo()))
	        .thenReturn(Optional.empty());

	    AtivoResponse response = buscarPorCodigoUseCase.executar(ativoPetr4.getCodigo());

	    Assert.assertNull(response.getId());
	    Assert.assertEquals(response.getCodigo(), ativoPetr4.getCodigo());
	    Assert.assertNull(response.getDescricao());

	    Assert.assertEquals(1, response.getResponse().getErros().size());
	    Assert.assertEquals(ListaErroEnum.ENTIDADE_NAO_ENCONTRADA, response.getResponse().getErros().get(0).getTipo());
	  }
	
}
