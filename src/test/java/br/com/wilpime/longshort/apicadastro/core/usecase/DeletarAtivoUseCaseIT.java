package br.com.wilpime.longshort.apicadastro.core.usecase;



import org.junit.Assert;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mockito;
import org.mockito.Spy;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.SpyBean;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.junit4.SpringRunner;

import br.com.six2six.fixturefactory.Fixture;
import br.com.six2six.fixturefactory.loader.FixtureFactoryLoader;
import br.com.thimilo.longshort.base.dto.response.ListaErroEnum;
import br.com.thimilo.longshort.base.gateway.DeletarGateway;
import br.com.wilpime.longshort.apicadastro.core.template.AtivoRequestTemplateLoader;
import br.com.wilpime.longshort.apicadastro.core.usecase.dto.AtivoResponse;
import br.com.wilpime.longshort.apicadastro.core.usecase.dto.AtivoRequest;
import br.com.wilpime.longshort.apicadastro.infra.dataprovider.jpa.repositoty.JpaAtivoRepository;

@RunWith(SpringRunner.class)
@SpringBootTest
@ActiveProfiles("test")
public class DeletarAtivoUseCaseIT {
	
	@Autowired
	private DeletarAtivoUseCase deletarUseCase;
	
	@Autowired
	private CadastrarAtivoUseCase cadastrarUseCase;
	
	@Autowired
	@Spy
	private JpaAtivoRepository repository;
	
	@SpyBean
	private DeletarGateway<String> deletarGateway;
	
	@Before
	public void before() {
		repository.deleteAll();
	}
	
	@BeforeClass
	public static void beforeClass() {
		FixtureFactoryLoader.loadTemplates("br.com.wilpime.longshort.apicadastro.core.template");
	}
	
	@Test
	public void deve_deletar_ativo() {
		AtivoRequest ativoPetr4 = Fixture.from(AtivoRequest.class).gimme(AtivoRequestTemplateLoader.PETR4);
		
		AtivoResponse response = cadastrarUseCase.executar(ativoPetr4);
		Assert.assertEquals(1, repository.count());
		AtivoResponse responseDeletar = deletarUseCase.executar(ativoPetr4.getCodigo());
		Assert.assertEquals(0, repository.count());
		
		Assert.assertEquals(response.getId(), responseDeletar.getId());
		Assert.assertEquals(response.getCodigo(), responseDeletar.getCodigo());
		Assert.assertEquals(response.getDescricao(), responseDeletar.getDescricao());

	}

	@Test
	public void nao_deve_deletar_ativo_inexistente() {
		
		AtivoResponse response = deletarUseCase.executar("AAAA");
		Assert.assertEquals(0, repository.count());
		
		Assert.assertNull(response.getId());
		Assert.assertEquals(response.getCodigo(), "AAAA");
		Assert.assertNull(response.getDescricao());
		Assert.assertEquals(1, response.getResponse().getErros().size());
		Assert.assertEquals(ListaErroEnum.ENTRADA_NAO_ENCONTRADA, response.getResponse().getErros().get(0).getTipo());
	}
	
	@Test
	public void nao_foi_possivel_deletar() {
		Mockito.when(deletarGateway.deletar(Mockito.anyString())).thenReturn(false);
		
		AtivoRequest ativoPetr4 = Fixture.from(AtivoRequest.class).gimme(AtivoRequestTemplateLoader.PETR4);
		AtivoResponse responseCadastrar = cadastrarUseCase.executar(ativoPetr4);
		Assert.assertEquals(1, repository.count());
		
		AtivoResponse responseDeletar = deletarUseCase.executar(responseCadastrar.getCodigo());
		Assert.assertEquals(1, repository.count());
		
		Assert.assertNull(responseDeletar.getId());
		Assert.assertEquals(responseDeletar.getCodigo(), ativoPetr4.getCodigo());
		Assert.assertNull(responseDeletar.getDescricao());
		Assert.assertEquals(1, responseDeletar.getResponse().getErros().size());
		Assert.assertEquals(ListaErroEnum.NAO_FOI_POSSIVEL_DELETAR, responseDeletar.getResponse().getErros().get(0).getTipo());
		
		
		
	}
}
