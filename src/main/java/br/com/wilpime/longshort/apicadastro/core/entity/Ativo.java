package br.com.wilpime.longshort.apicadastro.core.entity;

import br.com.thimilo.longshort.base.entity.BaseEntity;

public class Ativo extends BaseEntity {
	
	private String codigo;
	private String descricao;
	
	public String getCodigo() {
		return codigo;
	}
	public void setCodigo(String codigo) {
		this.codigo = codigo;
	}
	public String getDescricao() {
		return descricao;
	}
	public void setDescricao(String descricao) {
		this.descricao = descricao;
	}
	
	
	

}
