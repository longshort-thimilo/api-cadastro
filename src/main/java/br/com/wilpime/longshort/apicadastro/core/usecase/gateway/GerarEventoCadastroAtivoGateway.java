package br.com.wilpime.longshort.apicadastro.core.usecase.gateway;

import br.com.wilpime.longshort.apicadastro.core.usecase.dto.AtivoRequest;

public interface GerarEventoCadastroAtivoGateway {

	void gerarEvento(AtivoRequest ativo);
}
