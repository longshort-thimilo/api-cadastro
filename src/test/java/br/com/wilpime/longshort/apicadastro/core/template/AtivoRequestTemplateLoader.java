package br.com.wilpime.longshort.apicadastro.core.template;

import br.com.six2six.fixturefactory.Fixture;
import br.com.six2six.fixturefactory.Rule;
import br.com.six2six.fixturefactory.loader.TemplateLoader;
import br.com.wilpime.longshort.apicadastro.core.usecase.dto.AtivoRequest;

public class AtivoRequestTemplateLoader implements TemplateLoader {
	
	public static final String RANDOM = "RANDOM";
	public static final String PETR4 = "PETR4";
	
	@Override
	public void load() {
		Fixture.of(AtivoRequest.class).addTemplate(RANDOM,new Rule() {{
			add("codigo", firstName());
			add("descricao", name());
		}});
		Fixture.of(AtivoRequest.class).addTemplate(PETR4,new Rule() {{
			add("codigo", "PETR4");
			add("descricao", "PETOBRAS");
		}});
		
		
	}
	

}
