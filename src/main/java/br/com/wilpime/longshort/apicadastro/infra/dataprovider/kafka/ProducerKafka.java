package br.com.wilpime.longshort.apicadastro.infra.dataprovider.kafka;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.kafka.core.KafkaTemplate;
import org.springframework.stereotype.Component;

import br.com.wilpime.longshort.apicadastro.core.usecase.dto.AtivoRequest;
import br.com.wilpime.longshort.apicadastro.core.usecase.gateway.GerarEventoCadastroAtivoGateway;

@Component
public class ProducerKafka implements GerarEventoCadastroAtivoGateway {
	
	@Value(value = "${app.kafka.cadastraratitvos.topic-name}")
	private String kafkaTopicName;
	
	private final KafkaTemplate<String,AtivoRequest> kafkaTemplate;

	public ProducerKafka(KafkaTemplate<String, AtivoRequest> kafkaTemplate) {
		this.kafkaTemplate = kafkaTemplate;
	}

	@Override
	public void gerarEvento(AtivoRequest ativo) {
		kafkaTemplate.send(kafkaTopicName,ativo );
		
	}


}
