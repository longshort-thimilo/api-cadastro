package br.com.wilpime.longshort.apicadastro.infra.entrypoint.http.controller;

import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import br.com.thimilo.longshort.base.dto.response.BaseResponse;
import br.com.thimilo.longshort.base.dto.response.ListaErroEnum;
import br.com.thimilo.longshort.base.dto.response.ResponseDataErro;
import br.com.wilpime.longshort.apicadastro.core.usecase.AtualizarAtivoUseCase;
import br.com.wilpime.longshort.apicadastro.core.usecase.BuscarAtivosUseCase;
import br.com.wilpime.longshort.apicadastro.core.usecase.BuscarPorCodigoAtivoUseCase;
import br.com.wilpime.longshort.apicadastro.core.usecase.CadastrarAtivoUseCase;
import br.com.wilpime.longshort.apicadastro.core.usecase.DeletarAtivoUseCase;
import br.com.wilpime.longshort.apicadastro.core.usecase.dto.AtivoRequest;
import br.com.wilpime.longshort.apicadastro.core.usecase.dto.AtivoResponse;
import br.com.wilpime.longshort.apicadastro.core.usecase.dto.PaginadoAtivoResponse;

@RestController
@RequestMapping("/api/v1/ativos")
public class AtivoController {

	private final CadastrarAtivoUseCase cadastrarAtivoUseCase;
	private final DeletarAtivoUseCase deletarAtivoUseCase;
	private final BuscarPorCodigoAtivoUseCase buscarPorCodigoAtivoUseCase;
	private final AtualizarAtivoUseCase atualizarAtivoUseCase;
	private final BuscarAtivosUseCase buscarAtivosUseCase;
	private Logger log = LoggerFactory.getLogger(AtivoController.class);

	public AtivoController(CadastrarAtivoUseCase cadastrarAtivoUseCase, DeletarAtivoUseCase deletarAtivoUseCase,
			BuscarPorCodigoAtivoUseCase buscarPorCodigoAtivoUseCase, AtualizarAtivoUseCase atualizarAtivoUseCase, 
			BuscarAtivosUseCase buscarAtivosUseCase) {
		super();
		this.cadastrarAtivoUseCase = cadastrarAtivoUseCase;
		this.deletarAtivoUseCase = deletarAtivoUseCase;
		this.buscarPorCodigoAtivoUseCase = buscarPorCodigoAtivoUseCase;
		this.atualizarAtivoUseCase = atualizarAtivoUseCase;
		this.buscarAtivosUseCase = buscarAtivosUseCase;
	}

	@PostMapping(consumes = MediaType.APPLICATION_JSON_UTF8_VALUE, produces = MediaType.APPLICATION_JSON_UTF8_VALUE)
	public ResponseEntity<AtivoResponse> cadastrarAtivo(@RequestBody AtivoRequest request) {
		log.info("Cadastrando ativo " + request.getCodigo(), request);
		AtivoResponse response = cadastrarAtivoUseCase.executar(request);
		log.info("Ativo cadastrado, codigo = " + response.getCodigo() + "ID gerado =  " + response.getId(), response);
		return construirResponse(response);
	}

	// DELETE http://localhost:8080/api/v1/ativos/{id}
	// @DeleteMapping("{id}")
	@DeleteMapping(value = "/{codigo}", produces = MediaType.APPLICATION_JSON_UTF8_VALUE)
	public ResponseEntity<AtivoResponse> deletarAtivo(@PathVariable("codigo") String codigo) {
		log.info("Deletando Ativo " + codigo);
		AtivoResponse response = deletarAtivoUseCase.executar(codigo);
		return construirResponse(response);
	}

	private <T extends BaseResponse> ResponseEntity<T> construirResponse(T response) {

		log.debug("Iniciando a construção do response HTTP");

		if (response.getResponse().getErros().isEmpty()) {
			log.info("Resposta sem ERROS (200 - OK)", response);
			return ResponseEntity.ok(response);
		} else {
			log.error("Ocorreram erros ao processar sua requisição", response);
			List<ResponseDataErro> erros = response.getResponse().getErros();
			ResponseDataErro erro = erros.get(0);

			if (erro.getTipo().equals(ListaErroEnum.CAMPOS_OBRIGATORIOS)) {
				return ResponseEntity.status(HttpStatus.BAD_REQUEST).body(response);
			} else if (erro.getTipo().equals(ListaErroEnum.DUPLICIDADE)) {
				return ResponseEntity.status(HttpStatus.CONFLICT).body(response);
			} else if (erro.getTipo().equals(ListaErroEnum.ENTIDADE_NAO_ENCONTRADA)) {
				return ResponseEntity.status(HttpStatus.NOT_FOUND).body(response);
			} else {
				return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).body(response);
			}
		}

	}


	// METHOD: GET
	// URL: http://localhost:8080/api/v1/ativos/{codigo}
	@GetMapping(value = "/{codigo}", produces = MediaType.APPLICATION_JSON_UTF8_VALUE)
	@PreAuthorize("#oauth2.hasScope('read')")
	public ResponseEntity<AtivoResponse> buscarPorCodigo(@PathVariable("codigo") String codigo) {
		log.info("Buscando ativo por código " + codigo);

		AtivoResponse response = buscarPorCodigoAtivoUseCase.executar(codigo);

		log.info("Resposta da busca para o ativo de código " + response.getCodigo() + " - ID = " + response.getId(),
				response);

		return construirResponse(response);

	}
	
	// METHOD: GET
	// URL: http://localhost:8080/api/v1/ativos/{codigo}
	@GetMapping
	public ResponseEntity<PaginadoAtivoResponse> buscarAtivos(@RequestParam("pagina") Integer pagina) {
		
		PaginadoAtivoResponse response = buscarAtivosUseCase.executar(pagina);
		return construirResponse(response);

	}

	@PutMapping(value = "/{codigo}", consumes = MediaType.TEXT_PLAIN_VALUE, produces = MediaType.APPLICATION_JSON_UTF8_VALUE)
	public ResponseEntity<AtivoResponse> atualizarAtivo(@RequestBody String descricao,
			@PathVariable("codigo") String codigo) {
		AtivoRequest request = new AtivoRequest();
		request.setCodigo(codigo);
		request.setDescricao(descricao);
		log.info("Atualizando ativo " + request.getCodigo(), request);
		AtivoResponse response = atualizarAtivoUseCase.executar(request);
		log.info("Ativo atualizado, codigo = " + response.getCodigo() + "ID gerado =  " + response.getId(), response);
		return construirResponse(response);
	}

	@GetMapping("/public")
	public ResponseEntity<String> publicMethod(){
		return ResponseEntity.ok("Its Public");
	}
	
	// GET http://localhost:8080/api/v1/ativos/{codigo}
	// @GetMapping("{codigo}")

	// GET http://localhost:8080/api/v1/ativos
	// @GetMapping
}
