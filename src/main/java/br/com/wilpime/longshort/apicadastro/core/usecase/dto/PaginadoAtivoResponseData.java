package br.com.wilpime.longshort.apicadastro.core.usecase.dto;

public class PaginadoAtivoResponseData {
	
	private String id;
	private String codigo;
	private String descricao;
	
	public PaginadoAtivoResponseData() {
	}
	
	public PaginadoAtivoResponseData(String id, String codigo, String descricao) {
		super();
		this.id = id;
		this.codigo = codigo;
		this.descricao = descricao;
	}
	public String getId() {
		return id;
	}
	public void setId(String id) {
		this.id = id;
	}
	public String getCodigo() {
		return codigo;
	}
	public void setCodigo(String codigo) {
		this.codigo = codigo;
	}
	public String getDescricao() {
		return descricao;
	}
	public void setDescricao(String descricao) {
		this.descricao = descricao;
	}
	
	

}
