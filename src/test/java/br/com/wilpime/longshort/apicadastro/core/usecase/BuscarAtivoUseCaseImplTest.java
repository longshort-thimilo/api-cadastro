package br.com.wilpime.longshort.apicadastro.core.usecase;

import org.junit.Assert;
import org.junit.BeforeClass;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.MockitoJUnitRunner;

import br.com.six2six.fixturefactory.Fixture;
import br.com.six2six.fixturefactory.loader.FixtureFactoryLoader;
import br.com.wilpime.longshort.apicadastro.core.entity.Ativo;
import br.com.wilpime.longshort.apicadastro.core.template.AtivoTemplateLoader;
import br.com.wilpime.longshort.apicadastro.core.usecase.dto.PaginadoAtivoResponse;
import br.com.wilpime.longshort.apicadastro.core.usecase.dto.PaginadoAtivoResponseData;
import br.com.wilpime.longshort.apicadastro.core.usecase.gateway.BuscarAtivosGateway;
import br.com.wilpime.longshort.apicadastro.core.usecase.gateway.ConfiguracaoGateway;

@RunWith(MockitoJUnitRunner.class)
public class BuscarAtivoUseCaseImplTest {
	
	@InjectMocks
	private BuscarAtivosUseCaseImpl usecase;

	@Mock
	private BuscarAtivosGateway buscarAtivosGateway;

	@Mock
	private ConfiguracaoGateway configuracaoGateway;

	@BeforeClass
	public static void beforeClass() {
		FixtureFactoryLoader.loadTemplates("br.com.wilpime.longshort.apicadastro.core.template");
	}

	@Test
	public void deve_buscar_ativos_paginado() {
		final int quantidadeRegistrosPorPagina = 10;
		final int paginaAtual = 0;
		Mockito.when(configuracaoGateway.getQuantidadeRegistrosPorPagina()).thenReturn(quantidadeRegistrosPorPagina);

		PaginadoAtivoResponse responseMockado = new PaginadoAtivoResponse();

		for (int i = 0; i < quantidadeRegistrosPorPagina; i++) {
			Ativo ativo = Fixture.from(Ativo.class).gimme(AtivoTemplateLoader.RANDOM);
			responseMockado.adicionarAtivo(
					new PaginadoAtivoResponseData(ativo.getId(), ativo.getCodigo(), ativo.getDescricao()));
		}

		responseMockado.setPaginaAtual(paginaAtual);
		responseMockado.setQtdRegistroPagina(quantidadeRegistrosPorPagina);
		responseMockado.setQtdRegistoTotais(quantidadeRegistrosPorPagina);

		Mockito.when(buscarAtivosGateway.BuscarAtivos(paginaAtual, quantidadeRegistrosPorPagina)).thenReturn(responseMockado);

		PaginadoAtivoResponse response = usecase.executar(paginaAtual);

		Assert.assertNotNull(response);
		Assert.assertEquals(Integer.valueOf(paginaAtual), response.getPaginaAtual());
		Assert.assertEquals(Integer.valueOf(quantidadeRegistrosPorPagina), response.getQtdRegistroPagina());
		Assert.assertEquals(Integer.valueOf(quantidadeRegistrosPorPagina), response.getQtdRegistoTotais());
		Assert.assertEquals(quantidadeRegistrosPorPagina, response.getAtivos().size());
		Assert.assertEquals(0, response.getResponse().getErros().size());
	}
	
	@Test
	public void deve_buscar_ativos_paginado_mesmo_com_pagina_invalida() {
		final int quantidadeRegistrosPorPagina = 10;
		Mockito.when(configuracaoGateway.getQuantidadeRegistrosPorPagina()).thenReturn(quantidadeRegistrosPorPagina);

		PaginadoAtivoResponse responseMockado = new PaginadoAtivoResponse();

		for (int i = 0; i < quantidadeRegistrosPorPagina; i++) {
			Ativo ativo = Fixture.from(Ativo.class).gimme(AtivoTemplateLoader.RANDOM);
			responseMockado.adicionarAtivo(
					new PaginadoAtivoResponseData(ativo.getId(), ativo.getCodigo(), ativo.getDescricao()));
		}

		responseMockado.setPaginaAtual(0);
		responseMockado.setQtdRegistroPagina(quantidadeRegistrosPorPagina);
		responseMockado.setQtdRegistoTotais(quantidadeRegistrosPorPagina);

		Mockito.when(buscarAtivosGateway.BuscarAtivos(0, quantidadeRegistrosPorPagina)).thenReturn(responseMockado);

		PaginadoAtivoResponse response = usecase.executar(-10);

		Assert.assertNotNull(response);
		Assert.assertEquals(Integer.valueOf(0), response.getPaginaAtual());
		Assert.assertEquals(Integer.valueOf(quantidadeRegistrosPorPagina), response.getQtdRegistroPagina());
		Assert.assertEquals(Integer.valueOf(quantidadeRegistrosPorPagina), response.getQtdRegistoTotais());
		Assert.assertEquals(quantidadeRegistrosPorPagina, response.getAtivos().size());
		Assert.assertEquals(0, response.getResponse().getErros().size());
		
		response = usecase.executar(null);

		Assert.assertNotNull(response);
		Assert.assertEquals(Integer.valueOf(0), response.getPaginaAtual());
		Assert.assertEquals(Integer.valueOf(quantidadeRegistrosPorPagina), response.getQtdRegistroPagina());
		Assert.assertEquals(Integer.valueOf(quantidadeRegistrosPorPagina), response.getQtdRegistoTotais());
		Assert.assertEquals(quantidadeRegistrosPorPagina, response.getAtivos().size());
		Assert.assertEquals(0, response.getResponse().getErros().size());
	}
	

	
	
}
