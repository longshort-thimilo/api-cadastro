package br.com.wilpime.longshort.apicadastro.core.usecase;




import org.junit.Assert;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.junit4.SpringRunner;

import br.com.six2six.fixturefactory.Fixture;
import br.com.six2six.fixturefactory.loader.FixtureFactoryLoader;
import br.com.thimilo.longshort.base.dto.response.ListaErroEnum;
import br.com.wilpime.longshort.apicadastro.core.template.AtivoRequestTemplateLoader;
import br.com.wilpime.longshort.apicadastro.core.usecase.dto.AtivoResponse;
import br.com.wilpime.longshort.apicadastro.core.usecase.dto.AtivoRequest;
import br.com.wilpime.longshort.apicadastro.infra.dataprovider.jpa.repositoty.JpaAtivoRepository;

@RunWith(SpringRunner.class)
@SpringBootTest
@ActiveProfiles("test")
public class BuscaPorCodigoUseCaseIT {
	
	@Autowired
	private BuscarPorCodigoAtivoUseCase buscarPorCodigoUseCase;
	
	@Autowired
	private CadastrarAtivoUseCase cadastrarUseCase;
	
	@Autowired
	private JpaAtivoRepository repository;
		
	@Before
	public void before() {
		repository.deleteAll();
	}
	
	@BeforeClass
	public static void beforeClass() {
		FixtureFactoryLoader.loadTemplates("br.com.wilpime.longshort.apicadastro.core.template");
	}
	
	@Test
	public void deve_buscar_ativo_por_codigo() {
		AtivoRequest ativoPetr4 = Fixture.from(AtivoRequest.class).gimme(AtivoRequestTemplateLoader.PETR4);
		
		AtivoResponse ativoCadastrado = cadastrarUseCase.executar(ativoPetr4);
		Assert.assertEquals(1, repository.count());
		
		AtivoResponse response =  buscarPorCodigoUseCase.executar(ativoCadastrado.getCodigo());
		
		Assert.assertNotNull(response);
		Assert.assertEquals(ativoCadastrado.getCodigo(),response.getCodigo());
		Assert.assertEquals(ativoCadastrado.getId(),response.getId());
		Assert.assertEquals(ativoCadastrado.getDescricao(), response.getDescricao());
		Assert.assertEquals(0, response.getResponse().getErros().size());

	}
	
	  @Test
	  public void nao_deve_retornar_ativo_inexistente(){
		AtivoResponse response =  buscarPorCodigoUseCase.executar("PETR4");
	    
	    Assert.assertNotNull(response);
	    Assert.assertEquals("PETR4", response.getCodigo());
	    Assert.assertNull(response.getId());
	    Assert.assertNull(response.getDescricao());
	    Assert.assertEquals(1, response.getResponse().getErros().size());
	    Assert.assertEquals(ListaErroEnum.ENTIDADE_NAO_ENCONTRADA, response.getResponse().getErros().get(0).getTipo());
	  }
	  
}
