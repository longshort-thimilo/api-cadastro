package br.com.wilpime.longshort.apicadastro.core.usecase;

import java.util.Optional;

import org.apache.commons.lang.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;

import br.com.thimilo.longshort.base.dto.response.ListaErroEnum;
import br.com.thimilo.longshort.base.dto.response.ResponseDataErro;
import br.com.wilpime.longshort.apicadastro.core.entity.Ativo;
import br.com.wilpime.longshort.apicadastro.core.usecase.dto.AtivoResponse;
import br.com.wilpime.longshort.apicadastro.core.usecase.gateway.BuscarPorCodigoAtivoGateway;

@Service
public class BuscarPorCodigoUseCaseImpl implements BuscarPorCodigoAtivoUseCase {
	
	private final BuscarPorCodigoAtivoGateway buscarPorCodigoAtivoGateway;
	private Logger log = LoggerFactory.getLogger(BuscarPorCodigoUseCaseImpl.class);
	
	public BuscarPorCodigoUseCaseImpl(BuscarPorCodigoAtivoGateway buscarPorCodigoAtivoGateway) {
		this.buscarPorCodigoAtivoGateway = buscarPorCodigoAtivoGateway;
		
	}
	
	public AtivoResponse executar(String codigo) {
		log.info("Buscando ativo pelo código" + codigo);
		AtivoResponse response = new AtivoResponse();
		response.setCodigo(codigo);
		validarCamposObrigatorios(codigo,response);
		if(!response.getResponse().getErros().isEmpty()) {
			return response;
		}
		Optional<Ativo> opAtivo = buscarPorCodigoAtivoGateway.buscar(codigo);

		if (opAtivo.isPresent()) {
			response.setId(opAtivo.get().getId());
			response.setDescricao(opAtivo.get().getDescricao());
			
		}else {
			log.debug("Ativo encontrado com código"+ codigo);
			response.getResponse().adcionarErros(new ResponseDataErro("Ativo não encontrado com código"+ codigo, 
					ListaErroEnum.ENTIDADE_NAO_ENCONTRADA));
		}
		return response;
	}
	
	private void validarCamposObrigatorios(String codigo,AtivoResponse response ) {
		log.debug("validando campos obrigatórios");
		log.debug("código"+ codigo);
		if(StringUtils.isBlank(codigo)) {
			response.getResponse().adcionarErros(
					new ResponseDataErro("Campo código é obrigatório",ListaErroEnum.CAMPOS_OBRIGATORIOS));
		}
	}

}
