package br.com.wilpime.longshort.apicadastro.core.usecase;

import org.junit.Assert;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mockito;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.SpyBean;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.junit4.SpringRunner;

import br.com.six2six.fixturefactory.Fixture;
import br.com.six2six.fixturefactory.loader.FixtureFactoryLoader;
import br.com.wilpime.longshort.apicadastro.core.template.AtivoRequestTemplateLoader;
import br.com.wilpime.longshort.apicadastro.core.usecase.dto.AtivoRequest;
import br.com.wilpime.longshort.apicadastro.core.usecase.dto.PaginadoAtivoResponse;
import br.com.wilpime.longshort.apicadastro.core.usecase.gateway.ConfiguracaoGateway;
import br.com.wilpime.longshort.apicadastro.infra.dataprovider.jpa.repositoty.JpaAtivoRepository;


@RunWith(SpringRunner.class)
@SpringBootTest
@ActiveProfiles("test")
public class BuscarAtivoUseCaseIT {
	
	@Autowired
	private BuscarAtivosUseCase buscarAtivosUseCase;

	@Autowired
	private CadastrarAtivoUseCase cadastrarUseCase;

	@Autowired
	private JpaAtivoRepository repository;
	
	@SpyBean
	private ConfiguracaoGateway configuracaoGateway;

	@Before
	public void before() {
		repository.deleteAll();
		Mockito.when(configuracaoGateway.getQuantidadeRegistrosPorPagina()).thenReturn(2);
	}

	@BeforeClass
	public static void beforeClass() {
		FixtureFactoryLoader.loadTemplates("br.com.ronalan.longshort.apicadastro.template");
	}

	@Test
	public void deve_buscar_ativos_paginado() {
		
		for (int i = 0; i < 5; i++) {
			cadastrarUseCase.executar(Fixture.from(AtivoRequest.class).gimme(AtivoRequestTemplateLoader.RANDOM));
		}
		Assert.assertEquals(5, repository.count());
		
		PaginadoAtivoResponse paginadoResponse = buscarAtivosUseCase.executar(0);
		Assert.assertEquals(2, paginadoResponse.getAtivos().size());
		Assert.assertEquals(Integer.valueOf(0), paginadoResponse.getPaginaAtual());
		Assert.assertEquals(Integer.valueOf(2), paginadoResponse.getQtdRegistroPagina());
		Assert.assertEquals(Integer.valueOf(5), paginadoResponse.getQtdRegistoTotais());
		
		paginadoResponse = buscarAtivosUseCase.executar(2);
		Assert.assertEquals(1, paginadoResponse.getAtivos().size());
		Assert.assertEquals(Integer.valueOf(2), paginadoResponse.getPaginaAtual());
		Assert.assertEquals(Integer.valueOf(1), paginadoResponse.getQtdRegistroPagina());
		Assert.assertEquals(Integer.valueOf(5), paginadoResponse.getQtdRegistoTotais());
	}
	
	@Test
	public void deve_buscar_ativos_paginado_mesmo_com_pagina_invalida() {
		
		for (int i = 0; i < 5; i++) {
			cadastrarUseCase.executar(Fixture.from(AtivoRequest.class).gimme(AtivoRequestTemplateLoader.RANDOM));
		}
		Assert.assertEquals(5, repository.count());
		
		PaginadoAtivoResponse paginadoResponse = buscarAtivosUseCase.executar(-10);
		Assert.assertEquals(2, paginadoResponse.getAtivos().size());
		Assert.assertEquals(Integer.valueOf(0), paginadoResponse.getPaginaAtual());
		Assert.assertEquals(Integer.valueOf(2), paginadoResponse.getQtdRegistroPagina());
		Assert.assertEquals(Integer.valueOf(5), paginadoResponse.getQtdRegistoTotais());
		
		paginadoResponse = buscarAtivosUseCase.executar(null);
		Assert.assertEquals(2, paginadoResponse.getAtivos().size());
		Assert.assertEquals(Integer.valueOf(0), paginadoResponse.getPaginaAtual());
		Assert.assertEquals(Integer.valueOf(2), paginadoResponse.getQtdRegistroPagina());
		Assert.assertEquals(Integer.valueOf(5), paginadoResponse.getQtdRegistoTotais());
	}



}
