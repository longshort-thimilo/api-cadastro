package br.com.wilpime.longshort.apicadastro.infra.dataprovider.jpa.entity;

import java.time.LocalDateTime;
import java.util.Objects;

import javax.persistence.Column;
import javax.persistence.Id;
import javax.persistence.MappedSuperclass;

@MappedSuperclass
public abstract class JpaBaseEntity {
	
	@Id
	@Column(name= "id",length=36, nullable = false)
	private String id;
	
	@Column(name= "data_hora_criacao",nullable = false)
	private LocalDateTime dataHoraCriacao;

	@Override
	public int hashCode() {
		return Objects.hash(id);
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		JpaBaseEntity other = (JpaBaseEntity) obj;
		return Objects.equals(id, other.id);
	}

	
	public String getId() {
		return id;
	}

	
	public void setId(String id) {
		this.id = id;
	}

	
	public LocalDateTime getDataHoraCriacao() {
		return dataHoraCriacao;
	}

	
	public void setDataHoraCriacao(LocalDateTime dataHoraCriacao) {
		this.dataHoraCriacao = dataHoraCriacao;
	}
	
	

}
