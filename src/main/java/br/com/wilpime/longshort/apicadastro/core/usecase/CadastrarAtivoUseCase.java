package br.com.wilpime.longshort.apicadastro.core.usecase;


import br.com.thimilo.longshort.base.usecase.BaseUsecase;
import br.com.wilpime.longshort.apicadastro.core.usecase.dto.AtivoRequest;
import br.com.wilpime.longshort.apicadastro.core.usecase.dto.AtivoResponse;


public interface CadastrarAtivoUseCase extends BaseUsecase<AtivoRequest,AtivoResponse> {
	

}
