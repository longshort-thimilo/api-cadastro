package br.com.wilpime.longshort.apicadastro.core.usecase;


import org.junit.Assert;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mockito;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.SpyBean;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.junit4.SpringRunner;

import br.com.six2six.fixturefactory.Fixture;
import br.com.six2six.fixturefactory.loader.FixtureFactoryLoader;
import br.com.thimilo.longshort.base.dto.response.ListaErroEnum;
import br.com.wilpime.longshort.apicadastro.core.template.AtivoRequestTemplateLoader;
import br.com.wilpime.longshort.apicadastro.core.usecase.dto.AtivoRequest;
import br.com.wilpime.longshort.apicadastro.core.usecase.dto.AtivoResponse;
import br.com.wilpime.longshort.apicadastro.core.usecase.gateway.GerarEventoCadastroAtivoGateway;
import br.com.wilpime.longshort.apicadastro.infra.dataprovider.jpa.entity.JpaAtivoEntity;
import br.com.wilpime.longshort.apicadastro.infra.dataprovider.jpa.repositoty.JpaAtivoRepository;

@RunWith(SpringRunner.class)
@SpringBootTest
@ActiveProfiles("test")
public class CadastrarAtivoUseCaseIT {
	
	@Autowired
	private CadastrarAtivoUseCase cadastrarAtivoUseCase;
	
	@Autowired
	private AtualizarAtivoUseCase atualizarAtivoUseCase;
	
	@Autowired
	private JpaAtivoRepository repository;
	
	@SpyBean
	private GerarEventoCadastroAtivoGateway gerarEventoGateway;
	
	@Before
	public void before() {
		repository.deleteAll();
	}
	
	@BeforeClass
	public static void beforeClass() {
		FixtureFactoryLoader.loadTemplates("br.com.wilpime.longshort.apicadastro.core.template");
	}
	
	@Test
	public void deve_atualizar() {
		AtivoRequest request = Fixture.from(AtivoRequest.class)
				.gimme(AtivoRequestTemplateLoader.PETR4);
		
		Assert.assertEquals(0, repository.count());
		AtivoResponse response = cadastrarAtivoUseCase.executar(request);
		Assert.assertEquals(1, repository.count());
		
		AtivoRequest requestAtualizar = new AtivoRequest();
		requestAtualizar.setCodigo(response.getCodigo());
		requestAtualizar.setDescricao("Atualizei a descrição");
		AtivoResponse responseAtualizar = atualizarAtivoUseCase.executar(requestAtualizar);
		
		
		Assert.assertEquals(1, repository.count());
		Assert.assertNotNull(responseAtualizar.getId());
		Assert.assertEquals(requestAtualizar.getCodigo(), responseAtualizar.getCodigo());
		Assert.assertEquals(requestAtualizar.getDescricao(), responseAtualizar.getDescricao());
		Assert.assertNotEquals(request.getDescricao(), responseAtualizar.getDescricao());
		
		JpaAtivoEntity jpaAtivoEntity = repository.findAll().get(0);
		Assert.assertEquals(requestAtualizar.getDescricao(), jpaAtivoEntity.getDescricao());
		Mockito.verify(gerarEventoGateway).gerarEvento(Mockito.any());
		
	}
	
	@Test
	public void nao_deve_atualizar_ativo_campos_ausentes() {
		AtivoRequest request = new AtivoRequest();

		
		AtivoResponse response =  atualizarAtivoUseCase.executar(request);
		
		Assert.assertNotNull(response);
		Assert.assertNull(response.getId());

		Assert.assertNotNull(response.getResponse());
		
		Assert.assertEquals(2, response.getResponse().getErros().size());
		Assert.assertEquals(ListaErroEnum.CAMPOS_OBRIGATORIOS, response.getResponse().getErros().get(0).getTipo());
		Assert.assertEquals(ListaErroEnum.CAMPOS_OBRIGATORIOS, response.getResponse().getErros().get(1).getTipo());
//		Mockito.verify(gerarEventoGateway,Mockito.times(2)).gerarEvento(Mockito.any());

	}
	
	@Test
	public void nao_deve_atualizar_ativo_inexistente() {
		AtivoRequest request = Fixture.from(AtivoRequest.class).gimme(AtivoRequestTemplateLoader.PETR4);
		
		cadastrarAtivoUseCase.executar(request);
		Assert.assertEquals(1,repository.count());
		AtivoResponse response =  cadastrarAtivoUseCase.executar(request);
		
		Assert.assertNotNull(response);
		Assert.assertNull(response.getId());
		
		Assert.assertNotNull(response.getResponse());
		Assert.assertEquals(1,response.getResponse().getErros().size());
		Assert.assertEquals(ListaErroEnum.DUPLICIDADE,response.getResponse().getErros().get(0).getTipo());
		
		
		Assert.assertEquals(request.getCodigo(),response.getCodigo());
		Assert.assertEquals(request.getDescricao(), response.getDescricao());
		Assert.assertEquals(1, response.getResponse().getErros().size());
		
		Assert.assertEquals(1, repository.count());
		Mockito.verify(gerarEventoGateway,Mockito.times(1)).gerarEvento(Mockito.any());
		
		
	}

}
