package br.com.wilpime.longshort.apicadastro.core.usecase;

import org.springframework.stereotype.Service;

import br.com.thimilo.longshort.base.dto.response.ListaErroEnum;
import br.com.thimilo.longshort.base.dto.response.ResponseDataErro;
import br.com.thimilo.longshort.base.gateway.SalvarGateway;
import br.com.wilpime.longshort.apicadastro.core.entity.Ativo;
import br.com.wilpime.longshort.apicadastro.core.usecase.dto.AtivoRequest;
import br.com.wilpime.longshort.apicadastro.core.usecase.dto.AtivoResponse;
import br.com.wilpime.longshort.apicadastro.core.usecase.gateway.BuscarPorCodigoAtivoGateway;
import br.com.wilpime.longshort.apicadastro.core.usecase.gateway.GerarEventoCadastroAtivoGateway;

@Service
public class CadastrarAtivoUseCaseImpl  extends BaseAtivoUseCase implements CadastrarAtivoUseCase  {
	

	private final SalvarGateway<Ativo> gateway;
	private final BuscarPorCodigoAtivoGateway buscarPorCodigo;
	private final GerarEventoCadastroAtivoGateway cadastrarEventoGateway;
	
	public CadastrarAtivoUseCaseImpl(SalvarGateway<Ativo> gateway, BuscarPorCodigoAtivoGateway buscarPorCodigo,
			GerarEventoCadastroAtivoGateway cadastrarEventoGateway) {
		super();
		this.gateway = gateway;
		this.buscarPorCodigo = buscarPorCodigo;
		this.cadastrarEventoGateway= cadastrarEventoGateway;
	}

	@Override
	public AtivoResponse executar(AtivoRequest input) {
		
		AtivoResponse response = new AtivoResponse();
		response.setCodigo(input.getCodigo());
		response.setDescricao(input.getDescricao());
		this.validarCamposObrigatorio(input, response);
		
		if(response.getResponse().getErros().size() > 0) {
			return response;
		}
		
		if(buscarPorCodigo.buscar(input.getCodigo()).isPresent()) {
			response.getResponse().adcionarErros(new ResponseDataErro("Ativo já cadastrado com código " + input.getCodigo(),
					ListaErroEnum.DUPLICIDADE ));
		} else {
		Ativo ativo = new Ativo();
		ativo.setCodigo(input.getCodigo());
		ativo.setDescricao(input.getDescricao());
		ativo = gateway.salvar(ativo);
		cadastrarEventoGateway.gerarEvento(input);
		response.setId(ativo.getId());
		}
		return response;
	}
	

}
