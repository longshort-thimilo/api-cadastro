package br.com.wilpime.longshort.apicadastro.core.usecase.mapper;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.datatype.jsr310.JavaTimeModule;

public interface ObjectMapperUtilUseCase {
	
	static ObjectMapper mapper = new ObjectMapper(); 

	static <T> T  convertTo (Object value,Class<T> clazz) {
		mapper.registerModule(new JavaTimeModule());
		return (T) mapper.convertValue(value, clazz);
	}
}
