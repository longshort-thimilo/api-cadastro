package br.com.wilpime.longshort.apicadastro.core.usecase;


import java.util.Objects;


import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;


import br.com.wilpime.longshort.apicadastro.core.usecase.dto.PaginadoAtivoResponse;
import br.com.wilpime.longshort.apicadastro.core.usecase.gateway.BuscarAtivosGateway;
import br.com.wilpime.longshort.apicadastro.core.usecase.gateway.ConfiguracaoGateway;


@Service
public class BuscarAtivosUseCaseImpl implements BuscarAtivosUseCase {
	
	private final BuscarAtivosGateway buscarAtivosGateway;
	private final ConfiguracaoGateway configuracaoGateway;
	private Logger log = LoggerFactory.getLogger(BuscarAtivosUseCaseImpl.class);
	
		
	public BuscarAtivosUseCaseImpl(BuscarAtivosGateway buscarAtivosGateway,ConfiguracaoGateway configuracaoGateway) {
		this.buscarAtivosGateway = buscarAtivosGateway;
		this.configuracaoGateway = configuracaoGateway;
		
	}

	@Override
	public PaginadoAtivoResponse executar(Integer pagina) {
		log.info("Buscando ativos da página " + pagina + ". Qtd Registros máximo = " + configuracaoGateway.getQuantidadeRegistrosPorPagina());

		if (Objects.isNull(pagina) || pagina < 0) {
			log.warn("Estamos setando a página = 0, pois a página passada é igual a = " + pagina);
			pagina = 0;
		}

		PaginadoAtivoResponse response = buscarAtivosGateway.BuscarAtivos(pagina, configuracaoGateway.getQuantidadeRegistrosPorPagina());
		log.info("Foram encontrados [" + response.getQtdRegistroPagina() + "] registros na página " + pagina);
		log.info("Foram encontrados [" + response.getQtdRegistoTotais() + "] registros totais");

		return response;

	}

}
