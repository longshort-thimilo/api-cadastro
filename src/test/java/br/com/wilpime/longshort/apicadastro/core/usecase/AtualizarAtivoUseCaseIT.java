package br.com.wilpime.longshort.apicadastro.core.usecase;


import org.junit.Assert;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.junit4.SpringRunner;

import br.com.six2six.fixturefactory.Fixture;
import br.com.six2six.fixturefactory.loader.FixtureFactoryLoader;
import br.com.thimilo.longshort.base.dto.response.ListaErroEnum;
import br.com.wilpime.longshort.apicadastro.core.template.AtivoRequestTemplateLoader;
import br.com.wilpime.longshort.apicadastro.core.usecase.dto.AtivoRequest;
import br.com.wilpime.longshort.apicadastro.core.usecase.dto.AtivoResponse;
import br.com.wilpime.longshort.apicadastro.infra.dataprovider.jpa.entity.JpaAtivoEntity;
import br.com.wilpime.longshort.apicadastro.infra.dataprovider.jpa.repositoty.JpaAtivoRepository;

@RunWith(SpringRunner.class)
@SpringBootTest
@ActiveProfiles("test")
public class AtualizarAtivoUseCaseIT {
	
	@Autowired
	private CadastrarAtivoUseCase cadastrarAtivoUseCase;
	
	@Autowired
	private JpaAtivoRepository repository;
	
	@Before
	public void before() {
		repository.deleteAll();
	}
	
	@BeforeClass
	public static void beforeClass() {
		FixtureFactoryLoader.loadTemplates("br.com.wilpime.longshort.apicadastro.core.template");
	}
	
	@Test
	public void deve_salvar() {
		AtivoRequest request = Fixture.from(AtivoRequest.class)
				.gimme(AtivoRequestTemplateLoader.RANDOM);
		
		Assert.assertEquals(0, repository.count());
		AtivoResponse response = cadastrarAtivoUseCase.executar(request);
		Assert.assertEquals(1, repository.count());
		Assert.assertNotNull(response.getId());
		Assert.assertEquals(request.getCodigo(), response.getCodigo());
		Assert.assertEquals(request.getDescricao(), response.getDescricao());
		
		JpaAtivoEntity jpaAtivoEntity = repository.findAll().get(0);
		Assert.assertEquals(request.getCodigo(), response.getCodigo());
		Assert.assertEquals(request.getDescricao(), response.getDescricao());
		Assert.assertNotNull(jpaAtivoEntity.getDataHoraCriacao());
		Assert.assertNotNull(jpaAtivoEntity.getId());
		
	}
	
	@Test
	public void nao_deve_salvar_ativo_duplicado() {
		AtivoRequest request = Fixture.from(AtivoRequest.class).gimme(AtivoRequestTemplateLoader.PETR4);
		
		cadastrarAtivoUseCase.executar(request);
		Assert.assertEquals(1,repository.count());
		AtivoResponse response =  cadastrarAtivoUseCase.executar(request);
		
		Assert.assertNotNull(response);
		Assert.assertNull(response.getId());
		
		Assert.assertNotNull(response.getResponse());
		Assert.assertEquals(1,response.getResponse().getErros().size());
		Assert.assertEquals(ListaErroEnum.DUPLICIDADE,response.getResponse().getErros().get(0).getTipo());
		
		
		Assert.assertEquals(request.getCodigo(),response.getCodigo());
		Assert.assertEquals(request.getDescricao(), response.getDescricao());
		Assert.assertEquals(1, response.getResponse().getErros().size());
		
		Assert.assertEquals(1, repository.count());
		
		
	}
	
	@Test
	public void nao_deve_salvar_ativo_campos_ausentes() {
		AtivoRequest request = new AtivoRequest();
		request.setDescricao("        ");
		AtivoResponse response = cadastrarAtivoUseCase.executar(request);
		
		Assert.assertNotNull(response);
		Assert.assertNull(response.getId());
		
		Assert.assertNotNull(response.getResponse());
		Assert.assertEquals(2,response.getResponse().getErros().size());
		
		Assert.assertEquals(ListaErroEnum.CAMPOS_OBRIGATORIOS,response.getResponse().getErros().get(0).getTipo());
		Assert.assertEquals(ListaErroEnum.CAMPOS_OBRIGATORIOS,response.getResponse().getErros().get(1).getTipo());
		
		Assert.assertEquals(0, repository.count());

	}

}
