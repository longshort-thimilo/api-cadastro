package br.com.wilpime.longshort.apicadastro.core.usecase;

import java.util.Optional;

import org.junit.Assert;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.MockitoJUnitRunner;

import br.com.six2six.fixturefactory.Fixture;
import br.com.six2six.fixturefactory.loader.FixtureFactoryLoader;
import br.com.thimilo.longshort.base.dto.response.ListaErroEnum;
import br.com.thimilo.longshort.base.gateway.SalvarGateway;
import br.com.wilpime.longshort.apicadastro.core.entity.Ativo;
import br.com.wilpime.longshort.apicadastro.core.template.AtivoRequestTemplateLoader;
import br.com.wilpime.longshort.apicadastro.core.usecase.dto.AtivoRequest;
import br.com.wilpime.longshort.apicadastro.core.usecase.dto.AtivoResponse;
import br.com.wilpime.longshort.apicadastro.core.usecase.gateway.BuscarPorCodigoAtivoGateway;
import br.com.wilpime.longshort.apicadastro.core.usecase.gateway.GerarEventoCadastroAtivoGateway;

@RunWith(MockitoJUnitRunner.class)
public class CadastrarAtivoUseCaseImplTest {
	
	@InjectMocks
	private CadastrarAtivoUseCaseImpl cadastrarAtivoUseCase;
	
	@Mock
	private SalvarGateway<Ativo> gateway;
	
	@Mock
	private BuscarPorCodigoAtivoGateway buscarPorCodigoAtivoGateway;
	
	@Mock
	private GerarEventoCadastroAtivoGateway gerarEventoGateway;
	
	@Before
	public void before() {
		Mockito.when(gateway.salvar(Mockito.any())).thenAnswer(i -> i.getArgument(0));
	}
	
	@BeforeClass
	public static void beforeClass() {
		FixtureFactoryLoader.loadTemplates("br.com.wilpime.longshort.apicadastro.core.template");
	}
	
	@Test
	public void deve_salvar_ativo() {
		AtivoRequest request = Fixture.from(AtivoRequest.class).gimme(AtivoRequestTemplateLoader.RANDOM);
		AtivoResponse response =  cadastrarAtivoUseCase.executar(request);
		
		Assert.assertNotNull(response);
		Assert.assertNotNull(response.getId());
		Assert.assertEquals(request.getCodigo(),response.getCodigo());
		Assert.assertEquals(request.getDescricao(), response.getDescricao());
		Mockito.verify(gerarEventoGateway).gerarEvento(Mockito.any());

	}
	
	@Test
	public void nao_deve_salvar_ativo_duplicado() {
		AtivoRequest request = Fixture.from(AtivoRequest.class).gimme(AtivoRequestTemplateLoader.PETR4);
		Mockito.when(buscarPorCodigoAtivoGateway.buscar(request.getCodigo())).thenReturn(Optional.of(new Ativo()));
		
		AtivoResponse response =  cadastrarAtivoUseCase.executar(request);
		Assert.assertNotNull(response);
		Assert.assertNull(response.getId());
		
		Assert.assertNotNull(response.getResponse());
		Assert.assertEquals(1,response.getResponse().getErros().size());
		Assert.assertEquals(ListaErroEnum.DUPLICIDADE,response.getResponse().getErros().get(0).getTipo());
		
		
		Assert.assertEquals(request.getCodigo(),response.getCodigo());
		Assert.assertEquals(request.getDescricao(), response.getDescricao());
		Assert.assertEquals(1, response.getResponse().getErros().size());
		Mockito.verify(gerarEventoGateway,Mockito.times(0)).gerarEvento(Mockito.any());
		
	}
	
	@Test
	public void nao_deve_salvar_ativo_campos_ausentes() {
		AtivoRequest request = new AtivoRequest();
		request.setDescricao("        ");
		AtivoResponse response = cadastrarAtivoUseCase.executar(request);
		
		Assert.assertNotNull(response);
		Assert.assertNull(response.getId());
		
		Assert.assertNotNull(response.getResponse());
		Assert.assertEquals(2,response.getResponse().getErros().size());
		
		Assert.assertEquals(ListaErroEnum.CAMPOS_OBRIGATORIOS,response.getResponse().getErros().get(0).getTipo());
		Assert.assertEquals(ListaErroEnum.CAMPOS_OBRIGATORIOS,response.getResponse().getErros().get(1).getTipo());
		Mockito.verify(gerarEventoGateway,Mockito.times(0)).gerarEvento(Mockito.any());
		
	}



}
