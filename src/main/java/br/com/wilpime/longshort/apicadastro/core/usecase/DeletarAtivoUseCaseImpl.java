package br.com.wilpime.longshort.apicadastro.core.usecase;

import java.util.Optional;

import org.springframework.stereotype.Service;

import br.com.thimilo.longshort.base.dto.response.ListaErroEnum;
import br.com.thimilo.longshort.base.dto.response.ResponseDataErro;
import br.com.thimilo.longshort.base.gateway.DeletarGateway;
import br.com.wilpime.longshort.apicadastro.core.entity.Ativo;
import br.com.wilpime.longshort.apicadastro.core.usecase.dto.AtivoResponse;
import br.com.wilpime.longshort.apicadastro.core.usecase.gateway.BuscarPorCodigoAtivoGateway;

@Service
public class DeletarAtivoUseCaseImpl implements DeletarAtivoUseCase {

	private final BuscarPorCodigoAtivoGateway buscarPorCodigo;
	private final DeletarGateway<String> deletarGateway;

	public DeletarAtivoUseCaseImpl(BuscarPorCodigoAtivoGateway buscarPorCodigo,DeletarGateway<String> deletarGateway) {
		super();
		this.buscarPorCodigo = buscarPorCodigo;
		this.deletarGateway = deletarGateway;
	}

	@Override
	public AtivoResponse executar(String codigo) {
		AtivoResponse response = new AtivoResponse();
		response.setCodigo(codigo);
		Optional<Ativo> opAtivo =  buscarPorCodigo.buscar(codigo);
		if(opAtivo.isPresent()) {
			Boolean deletado = deletarGateway.deletar(codigo);
			if(deletado) {
				response.setId(opAtivo.get().getId());
				response.setDescricao(opAtivo.get().getDescricao());
				response.setCodigo(opAtivo.get().getCodigo());
			}else {
				response.getResponse().adcionarErros(new ResponseDataErro("Ativo do código "+ codigo +" não foi deletado. Favor contatar a equipe de suporte",
						ListaErroEnum.NAO_FOI_POSSIVEL_DELETAR));
			}
		}
		else {
			response.getResponse().adcionarErros(new ResponseDataErro("Ativo do código "+ codigo +" não existe",
					ListaErroEnum.ENTRADA_NAO_ENCONTRADA));
		}
		return response;

	}
	


}
