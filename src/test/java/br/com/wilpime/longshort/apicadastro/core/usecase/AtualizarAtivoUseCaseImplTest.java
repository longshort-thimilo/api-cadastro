package br.com.wilpime.longshort.apicadastro.core.usecase;

import java.util.Optional;

import org.junit.Assert;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.MockitoJUnitRunner;

import br.com.six2six.fixturefactory.Fixture;
import br.com.six2six.fixturefactory.loader.FixtureFactoryLoader;
import br.com.thimilo.longshort.base.dto.response.ListaErroEnum;
import br.com.thimilo.longshort.base.gateway.SalvarGateway;
import br.com.wilpime.longshort.apicadastro.core.entity.Ativo;
import br.com.wilpime.longshort.apicadastro.core.template.AtivoTemplateLoader;
import br.com.wilpime.longshort.apicadastro.core.usecase.dto.AtivoRequest;
import br.com.wilpime.longshort.apicadastro.core.usecase.dto.AtivoResponse;
import br.com.wilpime.longshort.apicadastro.core.usecase.gateway.BuscarPorCodigoAtivoGateway;

@RunWith(MockitoJUnitRunner.class)
public class AtualizarAtivoUseCaseImplTest {
	
	@InjectMocks
	private AtualizarAtivoUseCaseImpl atualizarAtivoUseCase;
	
	@Mock
	private SalvarGateway<Ativo> gateway;
	
	@Mock
	private BuscarPorCodigoAtivoGateway buscarPorCodigoAtivoGateway;
	
	@Before
	public void before() {
		Mockito.when(gateway.salvar(Mockito.any())).thenAnswer(i -> i.getArgument(0));

	}
	
	@BeforeClass
	public static void beforeClass() {
		FixtureFactoryLoader.loadTemplates("br.com.wilpime.longshort.apicadastro.core.template");
	}
	
	@Test
	public void deve_atualizar_ativo() {
		Ativo ativo = Fixture.from(Ativo.class).gimme(AtivoTemplateLoader.PETR4);
		String  descricaoAntiga = "PETOBRAS";
		Mockito.when(buscarPorCodigoAtivoGateway.buscar(ativo.getCodigo())).thenReturn(Optional.of(ativo));
		
		AtivoRequest request = new AtivoRequest();
		request.setCodigo(ativo.getCodigo());
		request.setDescricao("Atualizei descrição");
		
		AtivoResponse response =  atualizarAtivoUseCase.executar(request);
		
		Assert.assertNotNull(response);
		Assert.assertNotNull(response.getId());
		Assert.assertEquals(request.getCodigo(), response.getCodigo());
		Assert.assertNotEquals(descricaoAntiga,response.getDescricao());
		Assert.assertEquals("Atualizei descrição", response.getDescricao());

	}
	
	@Test
	public void nao_deve_salvar_ativo_campos_ausentes() {
		AtivoRequest request = new AtivoRequest();
		request.setDescricao("        ");
		AtivoResponse response = atualizarAtivoUseCase.executar(request);
		
		Assert.assertNotNull(response);
		Assert.assertNull(response.getId());
		
		Assert.assertNotNull(response.getResponse());
		Assert.assertEquals(2,response.getResponse().getErros().size());
		
		Assert.assertEquals(ListaErroEnum.CAMPOS_OBRIGATORIOS,response.getResponse().getErros().get(0).getTipo());
		Assert.assertEquals(ListaErroEnum.CAMPOS_OBRIGATORIOS,response.getResponse().getErros().get(1).getTipo());	
		
	}
	
	@Test
	public void nao_deve_atualizar_ativo_inexistente() {
		String  descricaoAntiga = "PETOBRAS";
		
		AtivoRequest request = new AtivoRequest();
		request.setCodigo("AA");
		request.setDescricao("Atualizei descrição");
		
		AtivoResponse response =  atualizarAtivoUseCase.executar(request);
		
		Assert.assertNotNull(response);
		Assert.assertNull(response.getId());
		Assert.assertEquals(request.getCodigo(), response.getCodigo());
		Assert.assertNotEquals(descricaoAntiga,response.getDescricao());
		Assert.assertEquals("Atualizei descrição", response.getDescricao());
		
		Assert.assertEquals(1, response.getResponse().getErros().size());
		Assert.assertEquals(ListaErroEnum.ENTIDADE_NAO_ENCONTRADA, response.getResponse().getErros().get(0).getTipo());

	}



}
