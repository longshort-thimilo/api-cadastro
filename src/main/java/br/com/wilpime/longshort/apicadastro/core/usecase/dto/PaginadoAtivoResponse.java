package br.com.wilpime.longshort.apicadastro.core.usecase.dto;

import java.util.ArrayList;
import java.util.List;

import br.com.thimilo.longshort.base.dto.response.BaseResponse;

public class PaginadoAtivoResponse extends BaseResponse {

	private Integer paginaAtual;
	private Integer qtdRegistroTotais;
	private Integer qtdRegistroPagina;
	private List<PaginadoAtivoResponseData> ativos = new ArrayList<>();

	public PaginadoAtivoResponse() {
	}

	public PaginadoAtivoResponse(Integer paginaAtual, Integer qtdRegistoTotais,Integer qtdRegistroPagina,
			List<PaginadoAtivoResponseData> ativos) {
		super();
		this.paginaAtual = paginaAtual;
		this.qtdRegistroPagina =qtdRegistroPagina;
		this.qtdRegistroTotais = qtdRegistoTotais;
		this.ativos = ativos;
	}

	public Integer getPaginaAtual() {
		return paginaAtual;
	}

	public void setPaginaAtual(Integer paginaAtual) {
		this.paginaAtual = paginaAtual;
	}

	public Integer getQtdRegistoTotais() {
		return qtdRegistroTotais;
	}

	public void setQtdRegistoTotais(Integer qtdRegistoTotais) {
		this.qtdRegistroTotais = qtdRegistoTotais;
	}

	public Integer getQtdRegistroPagina() {
		return qtdRegistroPagina;
	}

	public void setQtdRegistroPagina(Integer qtdRegistroPagina) {
		this.qtdRegistroPagina = qtdRegistroPagina;
	}

	public List<PaginadoAtivoResponseData> getAtivos() {
		return ativos;
	}

	public void adicionarAtivo(PaginadoAtivoResponseData ativoPaginado) {
		this.ativos.add(ativoPaginado);
	}

}
