package br.com.wilpime.longshort.apicadastro.core.usecase.gateway;

import java.util.Optional;

import br.com.wilpime.longshort.apicadastro.core.entity.Ativo;

public interface BuscarPorCodigoAtivoGateway {
	
	Optional<Ativo> buscar(String codigo);
}
