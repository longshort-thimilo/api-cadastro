package br.com.wilpime.longshort.apicadastro.core.usecase.gateway;

import br.com.wilpime.longshort.apicadastro.core.usecase.dto.PaginadoAtivoResponse;

public interface BuscarAtivosGateway {

	PaginadoAtivoResponse BuscarAtivos(Integer pagina, Integer qtdRegistroPagina);
}
