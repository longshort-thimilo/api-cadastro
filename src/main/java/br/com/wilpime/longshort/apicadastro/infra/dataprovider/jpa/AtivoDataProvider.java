package br.com.wilpime.longshort.apicadastro.infra.dataprovider.jpa;


import java.util.List;
import java.util.Objects;
import java.util.Optional;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.stereotype.Repository;

import br.com.thimilo.longshort.base.gateway.BuscarPorIdGateway;
import br.com.thimilo.longshort.base.gateway.DeletarGateway;
import br.com.thimilo.longshort.base.gateway.SalvarGateway;
import br.com.wilpime.longshort.apicadastro.core.entity.Ativo;
import br.com.wilpime.longshort.apicadastro.core.usecase.dto.PaginadoAtivoResponse;
import br.com.wilpime.longshort.apicadastro.core.usecase.dto.PaginadoAtivoResponseData;
import br.com.wilpime.longshort.apicadastro.core.usecase.gateway.BuscarAtivosGateway;
import br.com.wilpime.longshort.apicadastro.core.usecase.gateway.BuscarPorCodigoAtivoGateway;
import br.com.wilpime.longshort.apicadastro.infra.configuration.mapper.ObjectMapperUtil;
import br.com.wilpime.longshort.apicadastro.infra.dataprovider.jpa.entity.JpaAtivoEntity;
import br.com.wilpime.longshort.apicadastro.infra.dataprovider.jpa.repositoty.JpaAtivoRepository;

@Repository
public class AtivoDataProvider implements BuscarPorCodigoAtivoGateway, SalvarGateway<Ativo>,
		BuscarPorIdGateway<Ativo, String>, DeletarGateway<String>,BuscarAtivosGateway {

	private final JpaAtivoRepository repository;

	public AtivoDataProvider(JpaAtivoRepository repository) {
		this.repository = repository;
	}

	@Override
	public Optional<Ativo> BuscarPorId(String id) {
		Optional<JpaAtivoEntity> jpaAtivoEnity = repository.findById(id);
		if (Objects.nonNull(jpaAtivoEnity)) {
			return Optional.of(ObjectMapperUtil.convertTo(jpaAtivoEnity, Ativo.class));
		} else {
			return Optional.empty();
		}
	}

	@Override
	public Ativo salvar(Ativo ativo) {
		JpaAtivoEntity jpaAtivoEntity = repository.save(ObjectMapperUtil.convertTo(ativo, JpaAtivoEntity.class));
		return ObjectMapperUtil.convertTo(jpaAtivoEntity, Ativo.class);
	}

	@Override
	public Optional<Ativo> buscar(String codigo) {
		List<JpaAtivoEntity> jpaAtivos = repository.findByCodigo(codigo);
		if (jpaAtivos.isEmpty()) {
			return Optional.empty();
		} else {
			return Optional.of(ObjectMapperUtil.convertTo(jpaAtivos.get(0), Ativo.class));
		}
	}

	@Override
	public Boolean deletar(String codigo) {
		List<JpaAtivoEntity> jpaAtivos = repository.findByCodigo(codigo);
		if (!jpaAtivos.isEmpty()) {
			// TODO: fazer teste para simulação de erro
			repository.delete(jpaAtivos.get(0));
			return true;
		}
		return false;
	}

	@Override
	public PaginadoAtivoResponse BuscarAtivos(Integer pagina, Integer qtdRegistroPagina) {
		Page<JpaAtivoEntity> pageJpaAtivoEntity =  repository.findAll(PageRequest.of(pagina, qtdRegistroPagina));
		PaginadoAtivoResponse response = new PaginadoAtivoResponse();
		response.setPaginaAtual(pagina);
		response.setQtdRegistroPagina(pageJpaAtivoEntity.getNumberOfElements());
		response.setQtdRegistoTotais(Math.toIntExact(pageJpaAtivoEntity.getTotalElements()));
		pageJpaAtivoEntity.getContent().forEach(c -> response
				.adicionarAtivo(new PaginadoAtivoResponseData(c.getId(),c.getCodigo(),c.getDescricao())));
		return response;
	}


}
